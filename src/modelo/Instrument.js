
export class Instrument{

    constructor(tipoInstrumento,nombre){
     this._tipoInstrumento = tipoInstrumento;
     this._nombre = nombre;
    }

    get tipoInstrumento(){
        return this._tipoInstrumento;
    }
    set tipoInstrumento(tipoInstrumento){
        this._tipoInstrumento = tipoInstrumento;
    }

    get nombre(){
        return this.nombre;
    }
    set nombre(nombre){
        this._nombre = nombre;
    }

}
