import { Instrument } from './src/modelo/Instrument.js';

export class Piano extends Instrument{

    constructor(octaba,tipoPiano){
      super('Piano','Electrico');
      this._octava = octaba;
      this._tipoPiano = tipoPiano;
    }

    get octaba(){
        return this._octaba;
    }
    set octaba(octaba){
        this._octaba = octaba;
    }

    get tipoPiano(){
        return this.tipoPiano;
    }
    set tipoPiano(tipoPiano){
        this._tipoPiano = tipoPiano;
    }

}
