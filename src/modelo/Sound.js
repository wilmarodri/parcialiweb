
export class Sound {

  constructor(id,name,src){
            super(name);
            this._id=id;
            this._src = src;
            this._DOMElement = null;
            this.initDOMElement();
        }

        get id(){
            return this._src;
        }
        set id(id){
            this._id = id;
        }

        get src(){
            return this._src;
        }
        set src(src){
            this._src = src;
        }

        get DOMElement(){
            return this._DOMElement;
        }

        set DOMElement(el){
            this._DOMElement = el;
        }

        initDOMElement(){
            let audioEl = document.createElement("audio");
            audioEl.src = this._src;
            this._DOMElement = audioEl;
        }

        play(){
            this._DOMElement.play();
        }

        pause(){
            this._DOMElement.pause();
        }

}
