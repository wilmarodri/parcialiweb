﻿
/*
1. Antes de plantear una solución para Arropados ltda. Tenemos que definir y suponer una serie de requerimientos no funcionales (ya que solo es el diseño de la pagina), que de alguna manera nos permitirá ver algunas cosas del proceso de negocio y plantear una solución.

•	RNF01: el sitio web debe tener una página de inicio, donde se muestre los productos mas comprados en la plataforma, estos productos deben estar distribuidos como una cuadricula con un diseño polaroid y un box shadow, en la parte inferior de este diseño debe estar un nombre corto y el valor del producto.
•	RNF02: El sitio debe tener una barra lateral fija, donde debe tener dos elementos, los cuales son: un campo texto para poder hacer filtro de los productos por medio del nombre, y un combo box donde están almacenadas las categorías de los productos. Toda esta barra lateral va orientada a loa filtros de las búsquedas, explicados en los requisitos funcionales.
•	RNF03: El sitio web debe contar con una sección donde se exponga los datos principales de la empresa como, contacto, quienes somos,nuestros clientes y redes sociales.
•	RNF04: el sitio web debe ser totalmente resposive, y adecuarse a distintos dispositivos de visualización como smartphones, tablets o computadores.

A partir de los requisitos no funcionales anteriormente definidos plantearemos una solución en cuanto a tecnologías y diseño del sitio web.
Para el diseño de la pagina en general utilizaremos flex, esto debido a que es soportada por mas navegadores y de alguna manera tiende a ser un poco mas estable. En este caso la grilla no es una buena opción, debido a que no es soportada por varios navegadores y tiende a ser muy problemático para hacer un diseño resposive. Siguiendo por la línea del resposive, todo el sitio se implemetara con media queries, para asi ajustarlo a distintos dispositivos y resoluciones.
Entrando un poco mas a las tecnologías que utilizaremos en el  desarrollo de esta plataforma. Utilizaremos la herramientas básicas de desarrollo web como HTML5 y CSS3, yendo un poco mas a fondo utilizaremos la librería Jquery para hacer el sitio un poco mas interactivo, esto refiriéndonos a animaciones y a la experiencia de usuario. Para que de alguna manera reducir un poco el código en el CSS implementaremos SASS, ya que nos permite aplicar el concepto de programación DRY (don´t repeat yourself), hacer un código mas escalable y que de alguna manera pueda ser reutilizado.
Como ya es de conocimiento, el sitio web debe tener un dominio HTTPS, el cual va a ser cobrado al cliente en el costo del proyecto en general, este dominio será adquirido por medio de un proveedor de dominios. A esto hay que añadirle el alojamiento de la pagina en el servidor, este servicio lo daremos nosotros en el servidor de la empresa, esto con el objetivo de mantener un alojamiento seguro y estabilidad de la plataforma por medio del servidor.
Para la sección de la tienda virtual, se puede hacer una implementación por medio de Ecommerce, pero la propuesta solo está centrado en el FrontEnd de la página.

![layout_movil](https://drive.google.com/open?id=0B27dUTnu42IEQk5QRlpZeEdoaVk)
![layout_escritorio](https://drive.google.com/open?id=0B27dUTnu42IEanBkZ2lESU0wbnM)

2. En este punto vamos a proceder de la misma forma que en el punto anterior, vamos a definir una serie de requisitos, asociados al proyectos para partir de ahí hacer una definición de las tecnologías y herramientas que vamos a utilizar, teniendo en cuenta que el presupuesto para el proyecto no es muy elevado. Particularmente en este caso ya hay una plataforma hecha que solo funciona para computadores, entonces en este caso es modificar el diseño para que pueda ser visto en diferentes dispositivos y resoluciones, por este mismo motivo solo resultaría un requisito no funcional.

•	RNF01: la plataforma actual construida para la empresa SurrenderCo debe poder ser soportada en diferentes dispositivos como Smartphones, tables y computadores, a su vez en distintas resoluciones. Tambien teniendo una mejor experiencia de usuario, y la información de fácil acceso para el usuario.

como decía anteriormente, el presupuesto que destina la empresa para este proyecto no es muy alto, hay que tener en cuenta que lo referente al dominio y el servidor donde esta alojado el sitio ya esta resuelto, y también trabajaríamos sobre algo que ya esta hecho por tanto no ahorraría una parte del trabajo.
Basados en lo anterior habría que hacer uso de la tecnologías básicas de desarrollo web como HTML5 y CSS3, y yendo un poco mas hacia la experiencia de usuario usaríamos al librería de Jquery. Para solucionar el hecho de que la plataforma pueda ser vista en diferentes dispositivos, lo haríamos con media querie ya que nos permite definir la vista de un sitio dependiendo de la resolución o los dispositivos de visualización.
Para ahorrar un poco el código en el CSS y para aplicar el concepto de desarrollo DRY (don´t repeat yourself), utilizaríamos un preprocesador como SASS, que de alguna manera nos permite hacer un código escalable y reutilizable.
Para poder hacer de alguna forma mas fácil los accesos a la información contenida en la plataforma se puede hacer un replanteamiento de toda la organización de la misma y haciendo uso de la librería de Jquery se puede hacer de alguna manera una mejor experiencia de usuario a las personas que van a utilizar la plataforma.

3. POLYFILL:
Un polyfill es esencialmente el código que le permiten tener alguna funcionalidad específica que se puede esperar en los navegadores actuales o “modernos” a trabajar también en otros navegadores que no tienen el apoyo para que la funcionalidad incorporada.
Por lo tanto, se puede pensar en la técnica de polyfilling como un proceso de dos etapas en el que el primer paso es la detección de las funciones que están presentes en un navegador determinado, y luego “parches” en apoyo con scripts de ayuda (como JavaScript) para aquellas funciones que faltan en ese navegador.

Una aclaración de que queremos hacer: la práctica de polyfilling no se limita a añadir soporte para navegadores antiguos, pero en realidad también se aplica a los nuevos navegadores también. Esto es porque incluso los nuevos navegadores pueden no implementar todas las características en el estándar HTML5. Y si estamos escribiendo una aplicación que utiliza HTML5 HTML5 alguna característica que no está presente en la última versión de un popular navegador (Firefox o Chrome), entonces tenemos que añadir algo de código polyfill a cuidar de él.

Polyfill.js : 
Polyfill.js es una biblioteca diseñada para hacer escribir polyfills CSS mucho, mucho más fácil. Es una biblioteca de abstracción que se ocupa de la plantilla, por lo que puede centrarse en lo que hace su polyfill realmente.
Además, en la Web hoy en día, la mayoría de los polyfills estan aislados unos de otros, lo que significa que todos repiten las mismas caras tareas. Polyfill.js resuelve este problema. Proporciona una API común para que los autores de Polyfill se conecten, por lo que todo el trabajo duro sólo ocurre una vez como mucho. Las hojas de estilo se descargan, analizan y se almacenan en una caché para que las solicitudes adicionales no hagan doble trabajo.


EJEMPLO: 
Por ejemplo, la propiedad sessionStorage, que almacena datos para una sesión de usuario dado, es algo que hay de nuevo en HTML 5. Digamos que queremos comprobar para ver si esa propiedad está disponible “nativa” (que significa integrado en) en el navegador. Por lo tanto, podemos escribir algo de código Javascript como este para comprobar para ver si la propiedad sessionStorage se define:
typeof window.sessionStorage! == 'indefinido'
Sin embargo, un problema es que algunas versiones anteriores de Firefox pueden arrojar un error de excepción de seguridad cuando este código se ejecuta en el navegador Firefox. Así que si queremos evitar que la excepción de seguridad puedan ser lanzados, podemos simplemente envuelva esta pieza de código dentro de un bloque de intento de captura. Y si lo hacemos, nuestro código podría tener este aspecto:

/ * definimos la variable isThereSessionStorage que almacenará verdadero o falso * / 
var isThereSessionStorage = (function() {
 try {
    return typeof window.sessionStorage !== 'undefined';
  } catch (e) {
    return false;
  }
})(); 
if(!isThereSessionStorage)
// nuestro código polyfill va aquí ....

Y si el valor de la variable isThereSessionStorage es falsa, podemos ejecutar nuestro código polyfill (cualquiera que sea) para “llenar el hueco” de la propiedad que falta para que nuestra aplicación puede funcionar sin problemas incluso en este navegador.

4. TRANSPILAR:
Los transpiladores son herramientas que leen código fuente escrito en un lenguaje de programación y producen el código equivalente en otro lenguaje o a otra versión del mismo, siempre son lenguajes que están a un mismo nivel de abstracción, un ejemplo de esto es SASS ya que esta al mismo nivel de abstracción que CSS3, solo que nos ofrece más prestaciones, pero en conceptos generales es lo mismo. Ejemplos de estos transpiladores son:

•	TypeScript, CoffeScript, Dart a Java Script
•	SASS, LESS a CSS3

COMPILADOR: 

Un compilador es un programa informático que traduce un programa que ha sido escrito en un lenguaje de programación a un lenguaje común,1 usualmente lenguaje de máquina, aunque también puede ser traducido a un código intermedio (bytecode) o a texto. Este proceso de traducción se conoce como compilación.2
•	Compiladores cruzados: generan código para un sistema distinto del que están funcionando.
•	Compiladores optimizadores: realizan cambios en el código para mejorar su eficiencia, pero manteniendo la funcionalidad del programa original.
•	Compiladores de una sola pasada: generan el código máquina a partir de una única lectura del código fuente.
•	Compiladores de varias pasadas: necesitan leer el código fuente varias veces antes de poder producir el código máquina.
•	Compiladores JIT (Just In Time): forman parte de un intérprete y compilan partes del código según se necesitan.
INTERPRETADOR: interpretador es un programa informático capaz de analizar y ejecutar otros programas. Los intérpretes se diferencian de los compiladores o de los ensambladores en que mientras estos traducen un programa desde su descripción en un lenguaje de programación al código de máquina del sistema, los intérpretes sólo realizan la traducción a medida que sea necesaria, típicamente, instrucción por instrucción, y normalmente no guardan el resultado de dicha traducción.
•	Motor Zend
•	CPython
•	Ruby MRI
•	YARV

PREPROCESADOR:

 El preprocesador es un programa que procesa el código antes de que pase por el compilador. Funciona bajo el control de las líneas de comando del preprocesador denominadas directivas. Las directivas del preprocesador se colocan en el código fuente, normalmente en el principio del archivo. Antes de pasar por el compilador, el código fuente se examina por el preprocesador que detecta y ejecuta todas las directivas del preprocesador.
 
 5. ********************* TRANSPILADORES **************************

    ----------TYPESCRIPT-------

    VENTAJAS:

    •	¡Es completamente compatible con JavaScript! Eso quiere decir que puedes renombrar cualquier archivo .js a .ts e ir añadiendo tipos progresivamente. (Ni Dart, ni CoffeScript, ni HaXe son compatibles con Javascript).
    •	Añade clases, interfaces, módulos, multiples prototipos de función, closures con this real…
    •	¡Se puede usar hoy mismo!
    •	Hecho por el creador de C#, así que es bueno ya de base
    •	Funciona tanto en cliente en cualquier navegador como en el servidor con Node.JS
    •	Autocompletado y detección de errores desde ya

    DESVENTAJAS :

    •	Requiere un proceso de precompilación antes de poder usarse (nada que no se pueda automatizar)
    •	Al convertirse en JavaScript y no disponer de tipos, sigue con las mismas limitaciones en lo que respecta al tema de rendimiento (cosa que sí intenta mejorar Dart)

    ----------LESS, SASS, STYLUS-------

    VENTAJAS:

    •	Código más organizado,Al estar usando pre-procesadores puedes hacer un uso mejorado de la propiedad @import de CSS lo que te permite separar tu código en varios archivos para mejorar la organización del código
    •	Proyectos más fáciles de mantener,al poder separar tu código en múltilpes archivos resulta más fácil mantener tus proyectos
    •	Reutilización de código, el hecho de separar tu código en modulos independientes lo vuelve más reutilizable evitandote escribir muchas veces estilos que se repiten por todo el sitio.


    DESVENTAJAS:

    •	Deben aprender a usarse, no es que sea muy complicado aprender a usarlos, pero el hecho de tener que aprenderlo hace que algunos desarrolladores no les guste usarlos. 

**************************** COMPILADORES ****************************

      ---------- C -------

      VENTAJAS:

      Fácil de aprender, compacto, gran productividad, permite incorporar ASM dentro del código, gran cantidad de librerías, facil manejo de operaciones matemáticas, disminución del número de errores, más facil memorizar, lenguaje más cercano al humano, independencia de las distintas familias de micros

      DESVENTAJAS:

      Código poco optimizado, programas mas lentos, poco o ningun control sobre el código generado, mas dificil de depurar

      ---------- ASM -------

      VENTAJAS : 
      
      Código eficiente y rápido

      DESVENTAJAS :

      Curva de aprendizaje alta, facil equivocación, hay que escribir mucho código, necesidad de utlizar macros que complican el programa, no se puede embeber otro lenguaje, si no se conoce se pueden crear programas mas largos, lenguaje mas alejado del humano, lenguaje específico de las distintas familias de los micros
      
      ---------- BASIC -------

      VENTAJAS:

    Tiene algunas de las características del "C" por ser un lenguaje de mediano nivel


      DESVENTAJAS:

      Es un lenguage no estructurado que puede crear malos hábitos de programación, no tienen tantas librerias como otros lenguajes, es menos eficiente que el "C" y el "ASM"

      ---------- BASH -------

      VENTAJAS:

    •	Requiere pocos recursos, al manejarse solo con texto no necesita mucho procesamiento.
    •	Conciso y poderoso, bajo este paradigma un programa tiene muchísimas opciones que suelen estar ocultas, pero pueden ser accedidas mediante el comando correcto 
    •	Preferido por expertos, suele ser una tendencia. Lo que no quiere decir que siempre los expertos la emplean o que uno es experto por el simple hecho de usarlas.
    •	Facil de automatizar, gracias a que la interacción se basa en texto es posible unir varios comandos en uno formando procedimientos o scripts, es decir, pequeñas piezas de código con una función específica como ser organizar archivos, aplicar una edición rápida a un documento entre otras.

    DESVENTAJAS:

    Poco intuitiva, la principal razón por la cual suelen ser poco utilizadas es por la complejidad de uso. Normalmente un usuario no sabe qué hacer cuando se topa con este tipo de interfaces. La curva de aprendizaje es larga y muchas veces tediosa si la comparamos con las interfaces gráficas de usuario.

    No es rica visualmente, otra de las debilidades es que solo permite expresar cosas mediante texto plano. Lo que nos lleva a no poder visualizar imágenes, videos, trabajar en edición multimedia, etc.

    No es amistoso para los novatos, combinando estas tres desventajas formamos una más grande. Los usuarios primerizos no saben cómo trabajar salvo que hayan tenido instrucción previa o hayan leído el respectivo manual de referencia.

**************************** INTERPRETADORES ****************************

    VENTAJAS:

    •	Su principal ventaja es que permiten una fácil depuración. Permiten una mayor interactividad con el código en tiempo de desarrollo.
    •	En algunos lenguajes (Smalltalk, Prolog, LISP) está permitido y es frecuente añadir código según se ejecuta otro código, y esta característica solamente es posible implementarla en un intérprete.
    •	Puede ser interrumpido con facilidad.
    •	Puede ser rápidamente modificado y ejecutado nuevamente.
    •	Un Intérprete necesita menos memoria que un compilador.
    •	Facilita la búsqueda de errores.
    •	En algunos lenguajes está permitido añadir código según se ejecuta otro código.
    •	Menor consumo de memoria.

    DESVENTAJAS:

    •	Lentitud de ejecución, ya que al ejecutar a la vez que se traduce no puede aplicarse un alto grado de optimización.
    •	Cada instrucción debe ser traducida a código máquina tantas veces como sea ejecutada
    •	Durante la ejecución, el intérprete debe residir en memoria ya que no genera código objeto.



**************************** PREPROCESADORES ****************************

    VENTAJAS:

    •	Reusabilidad. Una comodidad de los lenguajes de programación que CSS, por sí solo, no ofrece. Por ejemplo, si varias clases tendrán el mismo color, podemos almacenar éste en una variable, evitando escribir varias veces el mismo valor en formatos incómodos como RGB o hexadecimal.
    •	Mejor sintaxis. Los mixins son conjuntos de propiedades que se agrupan bajo un mismo nombre. Por ejemplo,  si queremos que dos elementos tengan fondo color rojo, margen de 5 píxeles y texto amarillo, podemos englobar estas características bajo el nombre “miMixin” e incluir este nombre en la declaración de ambos elementos. Así, generamos un código más comprensible y menos repetitivo.
    •	Características dinámicas. Por ejemplo, podemos asignarle al ancho de un bloque un valor dinámico, expresado a través de una operación aritmética que pueda incluir variables. De esta manera, el ancho no tendrá un valor fijo en píxeles ni uno relativo a las dimensiones de la pantalla, sino que dependerá del factor que deseemos.

    DESVENTAJAS:

    •	Código complejo. El código ya compilado sí puede ser repetitivo, conteniendo una gran cantidad de líneas de código y agregando un importante peso al directorio de nuestro sitio web.
    •	Malas prácticas. Usar un metalenguaje de CSS sin conocer a fondo el verdadero lenguaje puede acostumbrar a los desarrolladores inexpertos a depender de herramientas de terceros.

    6. BUNDLE:

    consiste en proporcionar las aplicaciones en forma de paquetes. Estos paquetes están formados por los programas ejecutables de la aplicación, así como por todas las bibliotecas de las que depende y otros tipo de ficheros ,de forma que se proporcionan como un conjunto. Las bibliotecas de las que depende el programa pueden haber sido enlazadas tanto de forma dinámica como también estática. Por tanto, el usuario percibe que el paquete como un conjunto que representa al programa en sí, cuando en realidad incluye varios ficheros.

    El empaquetado de aplicaciones permite evitar los problemas de las dependencias tanto a la hora de instalar la aplicación como a la hora de usarla, ya que cada paquete lleva consigo sus dependencias, y la instalación o desinstalación de otro software no va a afectar a las dependencias de dicho paquete.
    
    La principal ventaja del empaquetado de aplicaciones es precisamente que se evitan la problemática de las dependencias, y que la aplicación se puede trasladar de un computador a otro sin necesidad de reinstalarla, ya que el paquete de la aplicación contiene todos los ficheros necesarios para ejecutarla. Sin embargo, como desventaja se presenta que estos paquetes ocupan mucho más espacio en el disco, especialmente si el paquete incluye bibliotecas.


*/
